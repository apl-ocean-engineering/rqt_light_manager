# Copyright 2024, UW-APL
# Author: Laura Lindzey

import rospy
import rostopic
from typing import Optional
import apl_msgs.msg

from qt_gui.plugin import Plugin

from python_qt_binding.QtWidgets import (
    QComboBox,
    QGridLayout,
    QHBoxLayout,
    QLabel,
    QLineEdit,
    QPushButton,
    QVBoxLayout,
    QWidget,
)

from python_qt_binding.QtGui import QIcon

from python_qt_binding.QtCore import QTimer

import threading
from typing import List, NamedTuple


class LightRow(NamedTuple):
    light_label: QLabel
    decrement_button: QPushButton
    light_level: QLabel  # Will have grey background if light totally off, white background if light is on
    increment_button: QPushButton
    cmd_label: QLabel
    cmd_text: QLineEdit
    cmd_button: QPushButton


class SealiteManager(Plugin):
    def __init__(self, context):
        super(SealiteManager, self).__init__(context)
        self.setObjectName("SealiteManager")

        # Will be initialized by user selecting box in GUI
        self.status_sub = None
        self.level_pub = None
        self.increment_pub = None

        self.setup_ui(context)
        self.channel_widgets = None

        self.update_timer = QTimer(self)
        self.update_timer.timeout.connect(self.timer_callback)
        self.update_timer.start(40)  # milliseconds

        # Whether the grid needs to be reset due to choosing a new status topic
        self.needs_reset = False
        # The next status message to be displayed. None if it has been processed.
        self.brightness_msg: Optional[apl_msgs.msg.LightBrightness] = None
        # Lock the message so it can't change underneath us while updating grid
        self.status_lock = threading.Lock()

    def save_settings(self, _plugin_settings, instance_settings):
        print("Called save_settings!")
        status_topic = self.sub_topics_combo_box.currentText()
        instance_settings.set_value("status_topic", status_topic)
        level_command_topic = self.level_pub_topics_combo_box.currentText()
        instance_settings.set_value("level_command_topic", level_command_topic)
        increment_command_topic = self.increment_pub_topics_combo_box.currentText()
        instance_settings.set_value("increment_command_topic", increment_command_topic)

    def restore_settings(self, _plugin_settings, instance_settings):
        print("Called restore_settings!")
        status_topic = instance_settings.value("status_topic")
        level_command_topic = instance_settings.value("level_command_topic")
        increment_command_topic = instance_settings.value("increment_command_topic")
        for topic, level_combo_box, increment_combo_box in zip(
            [status_topic, level_command_topic, increment_command_topic],
            [
                self.sub_topics_combo_box,
                self.level_pub_topics_combo_box,
                self.increment_pub_topics_combo_box,
            ],
        ):
            combo_box.addItem(topic)
            index = combo_box.findText(topic)
            combo_box.setCurrentIndex(index)
        self.sub_topic_changed()
        self.pub_topic_changed()

    def shutdown_plugin(self):
        if self.command_pub is not None:
            self.command_pub.unregister()

    def setup_ui(self, context):
        self.widget = QWidget()

        # Overall layout
        self.layout = QVBoxLayout()

        # Widgets for selecting topics
        self.topics_hbox = QHBoxLayout()
        self.update_topics_push_button = QPushButton()
        self.update_topics_push_button.setIcon(QIcon.fromTheme("view-refresh"))
        self.update_topics_push_button.clicked.connect(self.update_topics)
        self.sub_topics_combo_box = QComboBox()
        # Use `activated` rather than `currentIndexChanged`, because the later
        # will also be triggered when we're programmatically modifying the topic
        # list in response to a "Refresh" click.
        self.sub_topics_combo_box.activated.connect(self.sub_topic_changed)
        self.level_pub_topics_combo_box = QComboBox()
        self.level_pub_topics_combo_box.activated.connect(self.pub_topic_changed)
        self.sub_topics_combo_box.setSizeAdjustPolicy(QComboBox.AdjustToContents)
        self.level_pub_topics_combo_box.setSizeAdjustPolicy(QComboBox.AdjustToContents)
        self.sub_topics_label = QLabel("Status topic:")
        self.pub_topics_label = QLabel("Command topic:")
        self.topics_hbox.addWidget(self.update_topics_push_button)
        self.topics_hbox.addStretch(1)
        self.topics_hbox.addWidget(self.sub_topics_label)
        self.topics_hbox.addWidget(self.sub_topics_combo_box)
        self.topics_hbox.addStretch(1)
        self.topics_hbox.addWidget(self.pub_topics_label)
        self.topics_hbox.addWidget(self.level_pub_topics_combo_box)

        self.status_grid = QGridLayout()
        self.channel_label = QLabel("Channel")
        self.pin_label = QLabel("Pin")
        self.status_label = QLabel("Current State")
        self.command_label = QLabel("Turn On/Of")

        for label in [
            self.channel_label,
            self.pin_label,
            self.status_label,
            self.command_label,
        ]:
            label.setStyleSheet("font-weight: bold")

        self.status_grid.addWidget(self.channel_label, 0, 0)
        self.status_grid.addWidget(self.pin_label, 0, 1)
        self.status_grid.addWidget(self.status_label, 0, 2)
        self.status_grid.addWidget(self.command_label, 0, 3)

        self.on_color = "rgb(100, 190, 140)"
        self.off_color = "rgb(190, 100, 100)"

        self.layout.addLayout(self.topics_hbox)
        self.layout.addLayout(self.status_grid)
        self.layout.addStretch(1.0)

        self.widget.setLayout(self.layout)

        self.widget.setObjectName("SealiteManagerUI")
        # Show _widget.windowTitle on left-top of each plugin (when
        # it's set in _widget). This is useful when you open multiple
        # plugins at once. Also if you open multiple instances of your
        # plugin at once, these lines add number to make it easy to
        # tell from pane to pane.
        self.widget.setWindowTitle("SealiteManagerUI")
        if context.serial_number() > 1:
            self.widget.setWindowTitle(
                self.widget.windowTitle() + (" (%d)" % context.serial_number())
            )
        # Add widget to the user interface
        context.add_widget(self.widget)

    def timer_callback(self):
        if self.needs_reset:
            self.clear_grid()

        with self.status_lock:
            if self.brightness_msg is not None:
                self.update_grid(self.brightness_msg)
                self.brightness_msg = None

    def clear_grid(self):
        if self.channel_widgets is not None:
            # TODO: iterate through items in self.channel_widgets
            # and call item.deleteLater()
            self.channel_widgets = None
        self.needs_reset = False

    def update_topics(self):
        """
        Update the list of available topics for pub/sub.
        """
        print("Updating topics!")

        # Save state so we can restore it if possible after updating topics
        selected_pub_topic = self.level_pub_topics_combo_box.currentText()
        selected_sub_topic = self.sub_topics_combo_box.currentText()
        print(
            "Initially selected topics: sub: {}, pub: {}".format(
                selected_sub_topic, selected_pub_topic
            )
        )

        # We'll only publish to topics that somebody is subscribed to,
        # and only subscribe to topics that somebody is publishing.
        pub_topics = self.get_topics("apl_msgs/PowerCommand", subscribed=True)
        sub_topics = self.get_topics("apl_msgs/PowerStatus", subscribed=False)
        pub_topics.append("")
        sub_topics.append("")
        for topics, combo_box in zip(
            [pub_topics, sub_topics],
            [self.level_pub_topics_combo_box, self.sub_topics_combo_box],
        ):
            combo_box.clear()
            for topic in topics:
                print("Adding topic {}!".format(topic))
                combo_box.addItem(topic)

        for prev_selected, combo_box in zip(
            [selected_pub_topic, selected_sub_topic],
            [self.level_pub_topics_combo_box, self.sub_topics_combo_box],
        ):
            index = combo_box.findText(prev_selected)
            print(
                "Index of previously-selected topic ({}): {}".format(
                    prev_selected, index
                )
            )
            if index == -1:
                combo_box.addItem(prev_selected)
                index = combo_box.findText(prev_selected)
            print("Setting index to {}!".format(index))
            combo_box.setCurrentIndex(index)

    def get_topics(self, message_type: str, subscribed: bool) -> List[str]:
        """
        List all topics in the rosgraph matching the input message type.
        We're not using get_published_topics because it won't give us
        subscribed topics, which is what we care about for publishing commands.
        """
        selected_topics = []
        # Each return value will be a list of tuples, wehre eaach tuple is
        # ['/topic/name', 'msg_package/msg_type', ['node1', 'node2']]
        pubs, subs = rostopic.get_topic_list()
        if subscribed:
            available_topics = subs
        else:
            available_topics = pubs
        for topic_name, topic_type, _ in available_topics:
            if topic_type == message_type:
                selected_topics.append(topic_name)
        return selected_topics

    def sub_topic_changed(self):
        """
        Update subscriber to new topic.
        """

        if self.status_sub is not None:
            self.status_sub.unregister()
        sub_topic = self.sub_topics_combo_box.currentText()
        if sub_topic == "":
            return

        self.needs_reset = True

        print("Changing status topic to {}".format(sub_topic))
        # TODO(lindzey): This type should be turned into string for the combobox list
        self.status_sub = rospy.Subscriber(
            sub_topic, apl_msgs.msg.PowerStatus, self.power_status_callback
        )

    def power_status_callback(self, msg):
        with self.status_lock:
            self.brightness_msg = msg

    # TODO(lindzey): If "ON" is clicked but publisher not selected, pop up warning.
    def pub_topic_changed(self):
        """
        Update publisher to new topic.
        """
        if self.command_pub is not None:
            self.command_pub.unregister()
        pub_topic = self.level_pub_topics_combo_box.currentText()
        if pub_topic == "":
            return

        print("Changing command topic to {}".format(pub_topic))
        self.command_pub = rospy.Publisher(
            pub_topic, apl_msgs.msg.PowerCommand, queue_size=1
        )

    def send_command(self, channel, command):
        if self.command_pub is None:
            print("Asked to publish message without choosing output topic!")
            return
        print("Sending {} the command {}".format(channel, command))
        msg = apl_msgs.msg.PowerCommand()
        msg.header.stamp = rospy.Time.now()
        msg.channels = [channel]
        if command:
            msg.commands = [msg.ON]
        else:
            msg.commands = [msg.OFF]
        self.command_pub.publish(msg)

    def update_grid(self, msg):
        if self.channel_widgets is None:
            print("Building grid!")
            self.channel_widgets = {}
            row = 1
            for channel, pin, status in zip(msg.channels, msg.pins, msg.status):
                print("Adding widgets for channel: {}".format(channel))
                channel_label = QLabel(channel)
                self.status_grid.addWidget(channel_label, row, 0)
                pin_label = QLabel("{}".format(pin))
                self.status_grid.addWidget(pin_label, row, 1)
                if status:
                    status_label = QLabel("ON")
                    status_label.setStyleSheet(
                        "QLabel{background-color:" + self.on_color + "}"
                    )
                else:
                    status_label = QLabel("OFF")
                    status_label.setStyleSheet(
                        "QLabel{background-color:" + self.off_color + "}"
                    )
                self.status_grid.addWidget(status_label, row, 2)
                on_button = QPushButton("Turn ON")
                on_button.setStyleSheet(
                    "QPushButton{background-color:" + self.on_color + "}"
                )
                on_button.clicked.connect(
                    lambda event, channel=channel: self.send_command(channel, True)
                )
                self.status_grid.addWidget(on_button, row, 3)
                off_button = QPushButton("Turn OFF")
                off_button.setStyleSheet(
                    "QPushButton{background-color:" + self.off_color + "}"
                )
                off_button.clicked.connect(
                    lambda event, channel=channel: self.send_command(channel, False)
                )
                self.status_grid.addWidget(off_button, row, 4)
                self.channel_widgets[channel] = StatusRow(
                    channel_label, pin_label, status_label, on_button, off_button
                )
                row += 1
        else:
            for channel, status in zip(msg.channels, msg.status):
                status_text = self.channel_widgets[channel].status_label.text()
                if status and status_text != "ON":
                    self.channel_widgets[channel].status_label.setText("ON")
                    self.channel_widgets[channel].status_label.setStyleSheet(
                        "QLabel{background-color:" + self.on_color + "}"
                    )
                elif not status and status_text != "OFF":
                    self.channel_widgets[channel].status_label.setText("OFF")
                    self.channel_widgets[channel].status_label.setStyleSheet(
                        "QLabel{background-color:" + self.off_color + "}"
                    )
